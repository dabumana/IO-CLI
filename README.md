# IO-CLI
###### Initial Orchestation Command Line Interface (Docker/Compose).

  This tool help you to generate an initial structure and **configuration** file, you can setup your own environment, according to the requirements of your projet, please follow the instructions to complete the process of initial setup and then you can **build** your **stack** or **swarm** for production or test purposes.

#### Instructions:
---
######  Read the options below and select the operation that fits to your project.

- Enter the key number [ 1 ] > Default run for a single instance.

- Enter the key number [ 2 ] > First time run setup.
(This configure the initial steps and build of the compose.)

- Enter the key number [ 3 ] > Deploy swarm.

- Enter the key number [ 4 ] > Deploy stack.

- Enter the key number [ 5 ] > Prune system container.
**(WARNING: This option removes all the content on system container.)**

- Enter the key number [ 0 ] > Create and edit a compose config file.
###### (Defined environment with manual number of services with their preferences.)

> *Please remember that some options are just initial configuration.

### :octopus: Contribution:

This is an open source initiative, make your **contribution** and share some *kudos*.
