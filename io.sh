#!/bin/bash
#
# Author: Nicolás Álvarez (@namalvez)
# Project: Initial Orchestration Command Line Interface (IO-CLI)
# Repository: github.com/dabumana/io-cli.git
# Year: 2019
#
#Copyright (c)2019 Nicolás Álvarez (@namalvez)
#
#Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
#
#1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
#
#2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
#
#3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
#
#THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

function stack_conf() {
	echo '''
Stack configuration
-------------------
Define the path of the config file (Kubernetes or Docker-compose)

       1.Kubernetes
       2.Docker-compose

*Select one (1-2)
	'''
	read select
	case "$select" in
	    1)
		echo '''
Stack name (Kubernetes):
		'''
		read name
		echo '''
Define the path of the file (dir/to/file):
		'''
		read path
		docker stack deploy --compose-file "$path" "$name"
		;;
	    2)
		echo '''
Stack name (Docker-compose):
		'''
		read name
		echo '''
Define the path of the file (dir/to/file):
		'''
		read path		
		docker stack deploy --kubeconfig "$path" "$name"
		;;
	    *)
		echo '''
*** Unknow command... leaving session! ***
		'''
		exit 1
	esac
}

function compose() {
[ -f "docker-compose.yml" ] && rm docker-compose.yml || echo '''
Docker-Compose
--------------
'''
[ -d "services" ] && rm -rf services || mkdir services
echo '''
Fill the details of your config file.

Version:
'''
read version
echo "
version: '"$version"'" >> docker-compose.yml
echo '''
*Remember that some options are functional just for the version.
'''
echo "
networks:" >> docker-compose.yml
echo '''
Networks (number of networks to parent):
'''
read numnet
if [[ "$numnet" =~ ^[0-9]+$ ]]
then
    for i in $(seq 1 "$numnet");do
	echo '''
Network name:
	'''
	read name
	echo "
  "$name":
    driver: bridge" >> docker-compose.yml
	echo "
*** NETWORK ENABLE [ "$name" ] ***
	"
    done
else
    echo '''
*** NETWORK NOT DEFINED ***
    '''
fi
echo "
  default:
    driver: host" >> docker-compose.yml
echo "
volumes:" >> docker-compose.yml
echo '''
Volumes (number of volumes to parent with nodes):
'''
read numvol
if [[ "$numvol" =~ ^[0-9]+$ ]]
then
    for i in $(seq 1 "$numvol");do
	echo '''
Volume data name:
	'''
	read name
	echo "
  "$name": " >> docker-compose.yml
	echo "
*** VOLUME ENABLE [ "$name" ] ***
	"
    done     
else
    echo '''
*** VOLUMES NO DEFINED ***
'''
fi
echo '''
Service nodes (number of nodes with services to define):
'''
read quantity
echo "
services:" >> docker-compose.yml
if [[ "$quantity" =~ ^[0-9]+$ ]]
then
    for i in $(seq 1 "$quantity");do
	echo '''
Container name:    
    	'''
	read name
        echo '''
Container image:
		'''
	read image
	ENV=services/"$name"/
	[ ! -d ""$ENV"" ] && mkdir -p "$ENV" || echo '''
*Skiping folder creation

*** SERVICE DOCKER FOLDER EXIST ***
	''' 
	echo "
Provide the details of build and context.
-----------------------------------------

**BUILD
		
Context: "$ENV"
	"
	echo '''
Dockerfile:
	'''
	read dockerfile
	echo "
FROM "$image"
#COPY
#RUN
#CMD	
	" >> services/"$name"/"$dockerfile"
	echo '''
SHM size:
	'''
	read shm
	echo "
  "$name":
    image: "$image"
    build:
      context: "$ENV"
      dockerfile: "$dockerfile"" >> docker-compose.yml
	echo '''
**DEPLOY

Select mode:
       1. global (One container per swarm mode)
       2. replicated 
	'''
	read mode
	case "$mode" in
	    1)
		echo "
    deploy:
      mode: 'global' " >> docker-compose.yml
		echo "
*** DEPLOY MODE ENABLE [ global ] ***
		"
		;;
	    2)
		echo '''
Number of instances:
		'''
		read instances
		echo '''
Set the resources of each replica, limits and reservations.

### Limits
       
       cpu processors:
		'''
		read cpus
		echo '''
       memory (xG):
		'''
		read memory
		echo "
    deploy:
      mode: '"$mode"'
      replicas: "$instances"
      resources:
        limits:
          cpus: '"$cpus"'
          memory: "$memory"" >> docker-compose.yml
		if [[ "$version" == '3.7' ]]
		then
		    echo '''
### Reservations
       
       cpu processors:
		    '''
		    read cepusB
		    echo '''
       memory (xG):
		    '''
		    read memoryB		    
	  	    echo"
      reservations:
          cpus: '"$cpusB"'
          memory: "$memoryB"" >> docker-compose.yml
		    echo "
*** DEPLOY MODE ENABLE [ "$mode" ] *** 
		    "
		else
		    echo '''
*** VERSION WITHOUT RESOURCE RESERVATION ***
		    '''
		fi
		echo '''
Please select your restart policy in case of OOME (Out Of Memory Exceptions):

       1) none
       2) on-failure
       3) any

*Select the concurrence 
		'''
		read policy
		case "$policy" in
		    1)
			echo "
      restart_policy:
        condition: none " >> docker-compose.yml
			;;
		    2)
			echo "
      restart_policy:
        condition: on-failure
        delay: 15s
        max_attempts: 3
        window: 180s    " >> docker-compose.yml
			;;
		    3)
			echo "
      restart_policy:
        condition: any
        delay: 60s
        max_attempts: 3
        window: 120s    " >> docker-compose-yml
			;;
		    *)
			echo '''
*** UNKNOW OPTION ***
			'''
			;;
		esac
	        ;;
	    *)
		echo '''
*** Unknow command... default values added! ***
		'''
	        echo "
    deploy:
      mode: 'global'" >> docker-compose.yml
		;;
	esac
	echo "
Enter the DNS address for this container:
	"
	read dns
	echo "
    container_name: "$name"
    dns: "$dns"
    dns_search:" >> docker-compose.yml
	echo '''
Number of DNS resolvers:
	'''
	read resolver
	if [[ "$resolver" =~ ^[0-9]+$ ]]
	then
	    for i in $(seq 1 "$resolver");do
		echo '''
Address of DNS resolver:
		'''
		read addr
		echo "
      - "$addr"" >> docker-compose.yml
		echo "
*** DNS RESOLVER ENABLE [ "$addr" ] ***
		"
		done
	else
	    echo '''
*** DNS RESOLVER NOT DEFINED ***
	    '''
	fi
	echo '''
    volumes: ''' >> docker-compose.yml
	echo '''
Number of internal volumes: 
	'''
	read volumes
	if [[ "$volumes" =~ ^[0-9]+$ ]]
	then
	    for x in $(seq 1 "$volumes");do
		echo '''
Select the type of volume:

       1.volume
       2.bind		    
		'''
		read type
		case "$type" in
		    1)
			echo '''
Define the source path or name:
			'''
			read source
			echo '''
Define the destiny path:
			'''
			read path
			echo "
      - type: 'volume'
        source: "$source"
        target: "$path"
        volume:
          nocopy: true" >> docker-compose.yml
			echo "
*** INTERNAL VOLUME ENABLE [ "$source" ] ***
			"
			;;
		    2)
			echo '''
Define the source path or name:
			'''
			read source
			echo '''
Define the destiny path:
			'''
		        read path
			echo "
      - type: 'bind'
        source: "$source"
        target: "$path"" >> docker-compose.yml
			echo "
*** INTERNAL VOLUME ENABLE [ "$source" ] ***
			"
			;;
		    *)
			echo '''
*** Unknow type... No volume set ***
			'''
			;;
		esac
	    done
	else
	    echo '''
*** NO VOLUME SET DEFINED ***	
	    '''
	fi
	echo '''
PID number process:
        '''
	read pid
	echo "
    pid: '"$pid"'
    ports:" >> docker-compose.yml
        echo '''
How many ports do you need for this service:
	'''
	read many
	if [[ "$many" =~ ^[1-9]+$ ]]
	then
	    for x in $(seq 1 "$many");do
		echo '''
Target port:
		'''
		read target
		echo '''
Published port:
		'''
		read pub
		echo '''
Protocol of port:	
		'''
		read pro
		echo '''
Port mode:
		'''
		read mode
		echo "
      - target: "$target"
        published: "$pub"
        protocol: "$pro"
        mode: "$mode"" >> docker-compose.yml
		echo "
*** SERVICE PORT ENABLE [ "$target" ] ***
		"
	    done
	else
	    echo '''
*** NO PORT SET DEFINED ***
	    '''
	fi
	echo '''
    expose: ''' >> docker-compose.yml
	echo '''
Number of ports exposed in this service:
	'''
	read porq
	if [[ "$porq" =~ ^[1-9]+$ ]]
	then
	    for x in $(seq 1 "$porq");do
		echo '''
Exposed port:
		'''
		read expo
		echo "
      - "$expo"" >> docker-compose.yml
		echo "
*** PORT ENABLE EXPOSED [ "$expo" ] ***
		"
	    done
	else
	    echo '''
*** NO PORT EXPOSED ***
	    '''
	fi
	echo '''
    networks: ''' >> docker-compose.yml
	echo '''
Number of networks exposed in this service:
	'''
        read netq
	if [[ "$netq" =~ ^[1-9]+$ ]]
	then
	    for x in $(seq 1 "$netq");do
		echo '''
Enter the name of the network:
		'''
		read netn
		echo "
      - "$netn"" >> docker-compose.yml
		echo "
*** NETWORK ENABLE [ "$netn" ] ***
		"
	    done
	else
	    echo '''
** NO NETWORK SET DEFINED ***
	    '''
	fi
	echo '''
    depends_on: ''' >> docker-compose.yml
	echo '''
Number of dependant services:
	'''
	read serq
	if [[ "$serq" =~ ^[1-9]+$ ]]
	then
	    for x in $(seq 1 "$serq");do
		echo '''
Enter the dependant service name:
		'''
		read sern
		echo "
      - "$sern"" >> docker-compose.yml
		echo "
*** DEPENDENCY ENABLE [ "$sern" ] ***
		"
	    done
	else
	    echo '''
*** NO DEPENDANT SERVICES ***
	    '''
	fi
        echo "
*** NODE ENABLE [ "$i" ] ***
	"
    done
else
    echo '''
*** NO SERVICE SET DEFINED ***
    '''
fi
sed -i '/^$/d' docker-compose.yml 
exit 1
}

function editor() {
echo '''
Editor
======
Please select the values and provide details about your stack and fill the 
information to generate the config file.

'''
compose
}

function main(){
echo '''
				#####
				#I^O#
				#####

      Initial Orchestation Command Line Interface (Docker/Compose)

 This tool help you to generate an initial structure and *configuration* 
 file, you can setup your own environment, according to the requirements 
 of your project please follow the instructions to complete the process 
 of initial setup and then you can *build* your services for production
 or test purposes.

=========================================================================
Read the options below and select the operation that fits to your project.

Instructions:
     
     - Enter the key number [ 1 ] > Default run for a single instance 
       (This configures a single running instance.)
     
     - Enter the key number [ 2 ] > First time run setup
       (This configure the initial steps and build of the compose.)

     - Enter the key number [ 3 ] > Deploy swarm
       (Deploy swarm.)

     - Enter the key number [ 4 ] > Deploy stack
       (Deploy stack.)

     - Enter the key number [ 5 ] > Prune system container
       (WARNING: This option removes all the content on system container.)

     - Enter the key number [ 0 ] > Create and edit a compose config file
       (Defined environment with manual number of services with their 
       preferences.)

*Please remember that some options are just for initial configuration.

Enter your option:
'''
read key
case "$key" in
    0)
	echo '''
Please define the values and parameters in the editor to generate a new config
file for your deployment. 
	'''
	editor
	;;
    1)
	echo '''
Please define the path of the dockerfile to be executed (path/to/dockerfile):
	'''
	read path
	docker run -f "$path"
	;;
    2)
	docker-compose build
	docker-compose up
	echo '''
*** COMPOSE ENABLED ***
    	'''
	;;
    
    3)
	docker swarm ca
	docker swarm init
	echo '''
*** SWARM ENABLED ***
	'''
	;;
    4)
	stack_conf
	echo '''
*** STACK ENABLED ***
	'''
	;;
    5)
	docker-compose down -v --rmi all --remove-orphans
	docker system prune --volumes --all
	docker images prune
	docker container prune
	docker stack prune
	echo '''
*** DOCKER PRUNE ***
	'''
	exit 1
	;;
    *)
	echo '''
*** Unknow command... leaving session! ***
	'''
	exit 1
esac
}

main

